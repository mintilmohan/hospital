USE [Hospital]
GO
/****** Object:  Table [dbo].[Medicinemaster]    Script Date: 2021-08-22 01:11:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Medicinemaster](
	[MedId] [int] IDENTITY(1,1) NOT NULL,
	[medicine] [varchar](50) NOT NULL,
	[Quantity] [varchar](50) NULL,
	[Price] [varchar](50) NOT NULL,
	[userId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MedId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usermaster]    Script Date: 2021-08-22 01:11:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usermaster](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[LoginName] [varchar](50) NULL,
	[Password] [varchar](50) NOT NULL,
	[Email] [varchar](50) NULL,
	[TotalCnt] [varchar](50) NULL,
	[ContactNo] [varchar](50) NULL,
	[Address] [varchar](50) NULL,
	[IsApporved] [varchar](50) NULL,
	[Status] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[User_Login]    Script Date: 2021-08-22 01:11:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc  [dbo].[User_Login]  
@UserName varchar(50),  
@Password varchar(50)  
as begin  
    declare @UserId int =0,@TotalCnt int =0  
    select @UserId=UserId,@TotalCnt=TotalCnt from  Usermaster um   
    where LoginName=@UserName and Password=@Password 
    if(@TotalCnt>=5)  
    begin  
       select 0 UserId,'' UserName,'' LoginName,'' Password,'' Email,''
    end  
    if(@UserId>0)  
    begin  
        select UserId, UserName, LoginName, Password, Email from  Usermaster um   
        where UserId=@UserId   
        
      
    end  
    else  
    begin  
       Update Usermaster set @TotalCnt=TotalCnt+1    
       where LoginName=@UserName  
       select 0 UserId,'' UserName,'' LoginName,'' Password,''
    end  
    end 
GO
/****** Object:  StoredProcedure [dbo].[Usp_Login]    Script Date: 2021-08-22 01:11:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc  [dbo].[Usp_Login]  
@UserName varchar(50),  
@Password varchar(50)  
as begin  
    declare @UserId int =0,@TotalCnt int =0  
    select @UserId=UserId,@TotalCnt=TotalCnt from  Usermaster um   
    where LoginName=@UserName and Password=@Password and Status<>3 and IsApporved=1  
    if(@TotalCnt>=5)  
    begin  
       select 0 UserId,'' UserName,'' LoginName,'' Password,'' Email,'' ContactNo,   
    ''Address,0 IsApporved,-1 Status  
    end  
    if(@UserId>0)  
    begin  
        select UserId, UserName, LoginName, Password, Email, ContactNo,   
        Address, IsApporved, Status from  Usermaster um   
        where UserId=@UserId   
        
      
    end  
    else  
    begin  
       Update Usermaster set @TotalCnt=TotalCnt+1    
       where LoginName=@UserName and Status=1 and IsApporved=1  
       select 0 UserId,'' UserName,'' LoginName,'' Password,'' Email,'' ContactNo,   
    ''Address,0 IsApporved,0 Status  
    end  
    end 
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Studentmaster](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[LoginName] [varchar](50) NULL,
	[Password] [varchar](50) NOT NULL,
	[Email] [varchar](50) NULL,
	[ContactNo] [varchar](15) NULL,
	[Address] [varchar](50) NULL,
	[IsApporved] [int] NULL,
	[Status] [int] NULL,
	[TotalCnt] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

